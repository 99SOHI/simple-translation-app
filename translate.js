const translate = require('node-google-translate-skidz');
const prompt = require("prompt-sync")({ sigint: true });


console.log('Language List:\nhttps://cloud.google.com/translate/docs/languages\n')

const source = prompt("Source Language: ");
const target = prompt("Target Language: ")
const text = prompt("Text: ")

translate({
    text: text,
    source: source,
    target: target
  }, function(result) {
    console.log("\nResults:\n" + result.translation);
  });
